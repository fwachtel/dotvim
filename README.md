# Vim configuration files #

## Installation ##
Get back the repository :
~~~ sh
git clone git@bitbucket.org:fwachtel/dotvim.git .vim
~~~

Link vimrc and vimrc.bepo files to $HOME
~~~ sh
./install.sh
~~~

## Add new plugin ##
~~~ sh
git submodule add http://github.com/THEPLUGIN.git bundle/THEPLUGIN
~~~

## Update plugins ##
~~~ sh
./update.sh
~~~