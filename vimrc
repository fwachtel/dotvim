""" Autoreload .vimrc when changed
autocmd! bufwritepost .vimrc source %

""" Bépo handling
source ~/.vimrc.bepo

""" Pathogen
execute pathogen#infect()
set sessionoptions-=options

""" Generate doc
Helptags

""" Remove vi compatibility
set nocompatible


""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""

""" File type definition
filetype plugin on
au BufNewFile,BufRead *.m set filetype=matlab
au BufNewFile,BufRead *.md set filetype=markdown
au BufNewFile,BufRead *.cls set filetype=tex
au BufNewFile,BufRead *.tpl set filetype=cpp
au BufNewFile,BufRead *.frag,*.vert,*.fp,*.vp,*.glsl set filetype=glsl
au BufNewFile,BufRead *.sif set filetype=xml


""" Indentation
set autoindent
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

au FileType ledger setlocal noexpandtab


set list
set listchars=tab:→\ ,nbsp:░,trail:◄,extends:↔,precedes:↔
set backspace=indent,eol,start
set fillchars=vert:\|,fold:·,diff:-


""" Folding
function! NeatFoldText()
    let foldfill = matchstr(&fillchars, 'fold:\zs.')
    let txtcrop = substitute(getline(v:foldstart), '^\s\+\(\s[^\s]\)',
                    \ '\=repeat(foldfill, len(submatch(0))-2).submatch(1)', '')
    let line = strpart(txtcrop, 0, (winwidth(0)*2)/3) . ' '
    let linecount = ' +' . (v:foldend - v:foldstart + 1)
    let txtlen = strlen(substitute(line . linecount, '.', 'x', 'g'))
                    \+ &foldcolumn
                    \+ ((&number||&relativenumber) ? &numberwidth : 0)
    return line . repeat(foldfill, winwidth(0)-txtlen) . linecount
endfunction
set foldtext=NeatFoldText()
set foldcolumn=3
"set foldmethod=manual

let g:xml_syntax_folding=1
au FileType xml setlocal foldmethod=syntax

""" Automatic saving of folding
"au BufWinLeave * mkview
"au BufWinEnter * silent loadview


""" Syntax checking
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0


""" Save session options
"set sessionoptions=curdir,folds,tabpages,winsize

""" Persistent undo
set undofile
set undodir=.
set undolevels=1000
set undoreload=10000


""" Others
set helplang=fr
set history=100
set ruler
set number



""""""""""" Colors """""""""""

""" Color scheme
set background=dark
"set background=light
"colorscheme solarized
set t_Co=256
"colorscheme wombat
"colorscheme molokai
colorscheme zenburn

""" Line and column highlight
set cursorline
"hi CursorLine cterm=none ctermfg=none ctermbg=0
set nocursorcolumn
"hi CursorColumn cterm=none ctermfg=none ctermbg=0

""" Line number color
"hi LineNr ctermfg=8 ctermbg=black

""" Special character highlight
"hi SpecialKey ctermbg=8

""" Fold highlight
"hi Folded cterm=bold ctermbg=none
"hi FoldColumn cterm=none ctermbg=none

""" Search
"hi Search cterm=none ctermbg=1

""" Max 80 columns color
set textwidth=80
let &colorcolumn=join(range(81,300),",")
hi ColorColumn cterm=NONE ctermbg=238

""" Sign highlight
hi SignColumn ctermbg=238


""" Diff
"hi DiffAdd term=reverse cterm=bold ctermbg=green ctermfg=white
"hi DiffChange term=reverse cterm=bold ctermbg=cyan ctermfg=black
"hi DiffText term=reverse cterm=bold ctermbg=gray ctermfg=black
"hi DiffDelete term=reverse cterm=bold ctermbg=red ctermfg=black



""""""""""" F## Shortcuts """""""""""

" BufExplorer shortcut                                          ===>> MAP TO F2
nmap <F2> :BufExplorer<CR>

" Latex-box
""" toggle TOC frame                                            ===>> MAP TO F3
nmap <F3> :LatexTOCToggle<CR>
let g:tex_flavor='latex'
"autocmd FileType tex set spell wrap linebreak
" run latexmk in background an update continuously
let g:LatexBox_latexmk_preview_continuously=1
let g:LatexBox_quickfix=2

""" Rainbow parenthesis                                         ===>> MAP TO F4
nmap <F4> :RainbowParenthesesToggleAll<CR>

""" Gundo                                                       ===>> MAP TO F5
nmap <F5> :GundoToggle<CR>
let g:gundo_map_move_older = "t"
let g:gundo_map_move_newer = "s"

""" NERD Tree                                                   ===>> MAP TO F6
nmap <F6> :NERDTreeToggle<CR>
let NERDTreeWinSize=40
let NERDTreeWinPos="left"
let NERDTreeShowBookmarks=1
let NERDTreeMouseMode=2
"autocmd VimEnter * NERDTree

""" TagBar                                                      ===>> MAP TO F7
"nmap <F7> :TagbarToggle<CR>
"let g:tagbar_width=40
"autocmd VimEnter * TagbarOpen

""" Spelling
set nospell
syntax spell toplevel
""" french spelling                                             ===>> MAP TO F8
nmap <F8> :setlocal spell! spelllang=fr<CR>
""" american spelling                                           ===>> MAP TO F9
nmap <F9> :setlocal spell! spelllang=en_us<CR>
"hi clear SpellBad
"hi SpellBad   cterm=bold ctermbg=darkred
"hi clear SpellCap
"hi SpellCap   cterm=bold ctermbg=darkgreen
"hi clear SpellRare
"hi SpellRare   cterm=bold ctermbg=darkmagenta
"hi clear SpellLocal
"hi SpellLocal   cterm=bold ctermbg=darkyellow

""" TODO visualisation in current directory files (recursively) ===>> MAP TO F10
nmap <F10> :grep --exclude=*~ TODO **/* <CR> :cl <CR>



""" Airline """
set laststatus=2
"let g:airline_powerline_fonts = 1


""""""""""" TWEAKS """""""""""

""" SuperTab
"let g:SuperTabDefaultCompletionType = "<C-X><C-O>"
let g:SuperTabDefaultCompletionType = "context"


""" Alway stay centered                                         ===>> MAP TO zz
let &scrolloff=(&lines-8)/2 " start activated
nnoremap zz :let &scrolloff=(&lines-8)/2-&scrolloff<CR>


" searh selection with * (#)
vnoremap <silent> * :<C-U>
	\let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
	\gvy/<C-R><C-R>=substitute(
			\escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
	\gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
	\let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
	\gvy?<C-R><C-R>=substitute(
			\escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
	\gV:call setreg('"', old_reg, old_regtype)<CR>
