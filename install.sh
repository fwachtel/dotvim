# install dot file
ln -rs vimrc ~/.vimrc
ln -rs vimrc.bepo ~/.vimrc.bepo

# install vim plugin
git submodule init
git submodule update
